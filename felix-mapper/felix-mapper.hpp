#pragma once

#include <string>
#include <unordered_map>
#include <cstdint>

namespace felix {
  namespace mapper {
    class FelixMapper {
      public:
        FelixMapper(std::string path);
        uint64_t get_felix_id(const std::string& detector_resource_name);
        uint64_t get_felix_id(uint32_t detector_resource_id);

        std::string get_detector_resource_name(uint64_t felix_id);
        uint32_t get_detector_resource_id(uint64_t felix_id);

      private:
        void read_path(std::string path);
        void read_file(std::string path);
        std::string to_hex(uint64_t i);

        std::unordered_map<std::string, uint64_t> fid_by_name;
        std::unordered_map<uint32_t, uint64_t> fid_by_id;
        std::unordered_map<uint64_t, std::string> name_by_fid;
        std::unordered_map<uint64_t, uint32_t> id_by_fid;
    };
  }
}
