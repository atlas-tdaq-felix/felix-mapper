#include "felix-mapper/felix-mapper.hpp"

using namespace felix::mapper;

int main(int argc, char** argv) {
    if (argc < 2) {
        printf("Usage: felix-mapper-check <path>\n");
        exit(1);
    }

    FelixMapper mapper(argv[1]);

    return 0;
}
