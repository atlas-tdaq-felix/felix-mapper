#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "attr no fid" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_fid"),
    "No FelixID set for obj: FelixLink, id: ELink-102, file: test/data/no_fid/oks.xml");
}
