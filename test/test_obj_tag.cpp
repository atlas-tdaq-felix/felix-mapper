#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "obj no class" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_class"),
    "No 'class' attribute found in file: test/data/no_class/oks.xml");
}

TEST_CASE( "obj no id" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_id"),
    "No 'id' attribute found in file: test/data/no_id/oks.xml");
}
