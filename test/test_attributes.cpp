#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "attributes no name" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_name"),
    "No 'name' attribute found for obj: FelixLink, id: ELink-102, file: test/data/no_name/oks.xml");
}

TEST_CASE( "attributes no type" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_type"),
    "No 'type' attribute found for obj: FelixLink, id: ELink-102, file: test/data/no_type/oks.xml");
}

TEST_CASE( "attributes no val" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_val"),
    "No 'val' attribute found for obj: FelixLink, id: ELink-102, file: test/data/no_val/oks.xml");
}
