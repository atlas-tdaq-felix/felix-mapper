#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "oks no tag" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/no_oks"),
    "No <oks-data> tag found in file: test/data/no_oks/oks.xml");
}
