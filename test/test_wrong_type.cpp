#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "wrong name type" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/wrong_name_type"),
    "Type should be 'string' for attribute DetectorResourceName found for obj: FelixLink, id: ELink-102, file: test/data/wrong_name_type/oks.xml");
}

TEST_CASE( "wrong fid type" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/wrong_fid_type"),
    "Type should be 'u64' for attribute FelixId found for obj: FelixLink, id: ELink-102, file: test/data/wrong_fid_type/oks.xml");
}

TEST_CASE( "wrong id type" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/wrong_id_type"),
    "Type should be 'u32' for attribute DetectorResourceId found for obj: FelixLink, id: ELink-102, file: test/data/wrong_id_type/oks.xml");
}
