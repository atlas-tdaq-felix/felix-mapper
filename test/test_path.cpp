#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "path file" ) {
    felix::mapper::FelixMapper mapper("test/data/oks/oks.xml");

    uint64_t fid = 0x123456789abd3cef;
    std::string detector_resource_name = "AA/BB/CC/DD";
    uint32_t detector_resource_id = 0x12345678;

    REQUIRE( mapper.get_felix_id(detector_resource_name) == fid);
    REQUIRE( mapper.get_felix_id(detector_resource_id) == fid);
    REQUIRE( mapper.get_detector_resource_name(fid) == detector_resource_name);
    REQUIRE( mapper.get_detector_resource_id(fid) == detector_resource_id);

    REQUIRE_THROWS_AS(mapper.get_felix_id("non-existing"), std::out_of_range);
    REQUIRE_THROWS_AS(mapper.get_felix_id(0), std::out_of_range);
    REQUIRE_THROWS_AS(mapper.get_detector_resource_name(0), std::out_of_range);
    REQUIRE_THROWS_AS(mapper.get_detector_resource_id(0), std::out_of_range);

    felix::mapper::FelixMapper* mapper2;
    REQUIRE_THROWS_WITH(mapper2 = new felix::mapper::FelixMapper("non-existing-path"), "Path does not exists: non-existing-path");
}

TEST_CASE( "path directory" ) {
    felix::mapper::FelixMapper mapper("test/data/oks");

    // nsw-c
    uint64_t fid = 0x553456789abd3cef;
    std::string detector_resource_name = "NSW/C/CC/DD";
    uint32_t detector_resource_id = 0x55345678;

    REQUIRE( mapper.get_felix_id(detector_resource_name) == fid);
    REQUIRE( mapper.get_felix_id(detector_resource_id) == fid);
    REQUIRE( mapper.get_detector_resource_name(fid) == detector_resource_name);
    REQUIRE( mapper.get_detector_resource_id(fid) == detector_resource_id);
}
