#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers.hpp>

#include "felix-mapper/felix-mapper.hpp"

TEST_CASE( "duplicates names" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/duplicate_names"),
    "Duplicate detector_resource_name: NSW/A/CC/DD found for obj: FelixLink, id: ELink-A, file: test/data/duplicate_names/nsw-side-a.xml");
}

TEST_CASE( "duplicates fids" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/duplicate_fids"),
    "Duplicate felixid: 0x663456789abd3cef found for obj: FelixLink, id: ELink-A, file: test/data/duplicate_fids/nsw-side-a.xml");
}

TEST_CASE( "duplicates ids" ) {
    felix::mapper::FelixMapper* mapper;
    REQUIRE_THROWS_WITH(mapper = new felix::mapper::FelixMapper("test/data/duplicate_ids"),
    "Duplicate detector_resource_id: 0x66345678 found for obj: FelixLink, id: ELink-A, file: test/data/duplicate_ids/nsw-side-a.xml");
}
