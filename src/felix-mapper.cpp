#include <algorithm>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <vector>

#include "rapidxml/rapidxml.hpp"
#include "rapidxml/rapidxml_ext.hpp"
#include "felix-mapper/felix-mapper.hpp"

using namespace std;
using namespace felix::mapper;
using namespace rapidxml;

namespace fs = filesystem;

FelixMapper::FelixMapper(string path) {
    if (!fs::exists(path)) {
        throw runtime_error("Path does not exists: " + path);
    }

    read_path(path);
}

uint64_t FelixMapper::get_felix_id(const string& detector_resource_name) {
    return fid_by_name.at(detector_resource_name);
}

uint64_t FelixMapper::get_felix_id(uint32_t detector_resource_id) {
    return fid_by_id.at(detector_resource_id);
}

string FelixMapper::get_detector_resource_name(uint64_t felix_id) {
    return name_by_fid.at(felix_id);
}

uint32_t FelixMapper::get_detector_resource_id(uint64_t felix_id) {
    return id_by_fid.at(felix_id);
}

void FelixMapper::read_path(string path) {
    if (fs::is_directory(path)) {
        // order not given, so first read into vector and sort to make sure tests run correctly
        vector<string> paths;
        for(auto& p: fs::recursive_directory_iterator(path)) {
            path = p.path();
            if (!fs::is_directory(path)) {
                if (fs::path(path).extension() == ".xml") {
                    paths.push_back(path);
                }
            }
        }

        sort(paths.begin(), paths.end());
        for (auto path : paths) {
            read_file(path);
        }
    } else {
        read_file(path);
    }
}

void FelixMapper::read_file(string filename) {
    // cout << "Parsing file " << filename << endl;
    ifstream file(filename);

    xml_document<> doc;
    vector<char> buffer((istreambuf_iterator<char>(file)), istreambuf_iterator<char>( ));
    buffer.push_back('\0');
    doc.parse<0>(&buffer[0]);
    xml_node<>* oks_data = doc.first_node("oks-data");

    if (oks_data == nullptr) {
      throw runtime_error("No <oks-data> tag found in file: " + filename);
    }
    // cout << oks_data << endl;
    // print(cout, doc, 0);

    for (xml_node<> * obj_node = oks_data->first_node("obj"); obj_node; obj_node = obj_node->next_sibling()) {
        // handle obj tag
	    	string clazz = "";
        string id = "";
        for (xml_attribute<> *attr = obj_node->first_attribute(); attr; attr = attr->next_attribute()) {
            std::string attr_name = attr->name();
            if (attr_name == "class") {
                clazz = attr->value();
            } else if (attr_name == "id") {
                id = attr->value();
            }
        }
        if (clazz == "") {
            throw runtime_error("No 'class' attribute found in file: " + filename);
        }
        if (id == "") {
            throw runtime_error("No 'id' attribute found in file: " + filename);
        }

        // handle attr tags
        uint64_t fid = 0;
        bool fid_set = false;
        string detector_resource_name = "";
        uint32_t detector_resource_id = 0;
        bool detector_resource_id_set = false;
        for (xml_node<> * attr_node = obj_node->first_node("attr"); attr_node; attr_node = attr_node->next_sibling()) {
            string name = "";
            string type = "";
            string val = "";
            for (xml_attribute<> *attr = attr_node->first_attribute(); attr; attr = attr->next_attribute()) {
                std::string attr_name = attr->name();
                if (attr_name == "name") {
                    name = attr->value();
                } else if (attr_name == "type") {
                    type = attr->value();
                } else if (attr_name == "val") {
                    val = attr->value();
                }
            }
            if (name == "") {
                throw runtime_error("No 'name' attribute found for obj: " + clazz + ", id: " + id + ", file: " + filename);
            }
            if (type == "") {
                throw runtime_error("No 'type' attribute found for obj: " + clazz + ", id: " + id + ", file: " + filename);
            }
            if (val == "") {
                throw runtime_error("No 'val' attribute found for obj: " + clazz + ", id: " + id + ", file: " + filename);
            }

            // printf("%s %s %s\n", name.c_str(), type.c_str(), val.c_str());
            if (name == "FelixId") {
                if (type != "u64") {
                    throw runtime_error("Type should be 'u64' for attribute " + name + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                fid = stoul(val, 0, 0);
                fid_set = true;
            } else if (name == "DetectorResourceId") {
                if (type != "u32") {
                    throw runtime_error("Type should be 'u32' for attribute " + name + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                detector_resource_id = stoul(val, 0, 0);
                detector_resource_id_set = true;
            } else if (name == "DetectorResourceName") {
                if (type != "string") {
                    throw runtime_error("Type should be 'string' for attribute " + name + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                detector_resource_name = val;
            }
        }

        if (fid_set) {
            if (detector_resource_name != "") {
                if (fid_by_name.count(detector_resource_name) > 0) {
                    throw runtime_error("Duplicate detector_resource_name: " + detector_resource_name + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                fid_by_name[detector_resource_name] = fid;

                if (name_by_fid.count(fid) > 0) {
                    throw runtime_error("Duplicate felixid: 0x" + to_hex(fid) + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                name_by_fid[fid] = detector_resource_name;
            }
            if (detector_resource_id_set) {
                if (fid_by_id.count(detector_resource_id) > 0) {
                    throw runtime_error("Duplicate detector_resource_id: 0x" + to_hex(detector_resource_id) + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                fid_by_id[detector_resource_id] = fid;

                if (id_by_fid.count(fid) > 0) {
                    throw runtime_error("Duplicate felixid: 0x" + to_hex(fid) + " found for obj: " + clazz + ", id: " + id + ", file: " + filename);
                }
                id_by_fid[fid] = detector_resource_id;
            }
        } else {
            throw runtime_error("No FelixID set for obj: " + clazz + ", id: " + id + ", file: " + filename);
        }
    }
}

string FelixMapper::to_hex(uint64_t i) {
    stringstream sstream;
    sstream << hex << i;
    return sstream.str();
}
